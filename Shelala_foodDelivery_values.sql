
USE shelala;

INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone)
            VALUES('Sherise', 'Lam', 'sherise97@gmail.com', '1301 Wharfside Point South', 'London', 'E14 9EX', 'UK', '07741469957');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone)
            VALUES('Lola', 'Dada', 'dadalola45@gmail.com', '1 Victoria Road', 'London', 'W3 6FA', 'UK', '07744933975');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone) VALUES('Maria', 'Anders', NULL, 'Obere Str. 57', 'Berlin', '12209', 'Germany', '030-0074321');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone) VALUES('Ana', 'Trujillo', NULL, 'Avda. de la Constitución 2222', 'México D.F.', '05021', 'Mexico', '(5) 555-4729');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone) VALUES('Antonio', 'Moreno', NULL, 'Mataderos  2312', 'México D.F.', '05023', 'Mexico', '(5) 555-3932');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone) VALUES('Thomas', 'Hardy', NULL, '120 Hanover Sq.', 'London', 'WA1 1DP', 'UK', '(171) 555-7788');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone) VALUES('Christina', 'Berglund', NULL, 'Berguvsvägen  8', 'Luleå', 'S-958 22', 'Sweden', '0921-12 34 65');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone) VALUES('Hanna', 'Moos', NULL, 'Forsterstr. 57', 'Mannheim', '68306', 'Germany', '0621-08460');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone) VALUES('Frédérique', 'Citeaux', NULL, '24, place Kléber', 'Strasbourg', '67000', 'France', '88.60.15.31');
INSERT INTO Customers (FirstName, LastName, EmailAddress, Address, City, PostalCode, Country, Phone) VALUES('Martín', 'Sommer', NULL, 'C/ Araquil, 67', 'Madrid', '28023', 'Spain', '(91) 555 22 82');

INSERT INTO Restaurants (RestaurantName, Cuisine, Address, City, PostalCode, Country, Phone, HomePage) VALUES('The Food Playground', 'Mix', 'City Center Plaza 516 Main St.', 'London', NULL, 'UK', '(9) 331-6954', NULL);
INSERT INTO Restaurants (RestaurantName, Cuisine, Address, City, PostalCode, Country, Phone, HomePage) VALUES('Cookaholic', 'Greek', '8 Johnstown Road', 'London', NULL, 'UK', '035-640230', NULL);
INSERT INTO Restaurants (RestaurantName, Cuisine, Address, City, PostalCode, Country, Phone, HomePage) VALUES('School Lunch', 'Korean', '14 Garrett Hill', 'London', NULL, 'UK', '(514) 555-8055', NULL);
INSERT INTO Restaurants (RestaurantName, Cuisine, Address, City, PostalCode, Country, Phone, HomePage) VALUES('Chinese Takeaway', 'Chinese', '7 Houndstooth Rd.', 'London', NULL, 'UK', '0522-556722', NULL);
INSERT INTO Restaurants (RestaurantName, Cuisine, Address, City, PostalCode, Country, Phone, HomePage) VALUES('Happy Western Dine', 'Western', '90 Wadhurst Rd.', 'London', NULL, 'UK', '0221-0765721', NULL);
INSERT INTO Restaurants (RestaurantName, Cuisine, Address, City, PostalCode, Country, Phone, HomePage) VALUES('Oiishi Doki', 'Japanese', '92 Setsuko Chuo-ku', 'Osaka', NULL, 'Japan', '(06) 431-7877', NULL);

INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Patricio', 'Simpson', '(1) 135-5555', '4.5');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Francisco', 'Chang', '(5) 555-3392', '4.2');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Pedro', 'Afonso', '(11) 555-7647', '4.7');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Aria', 'Cruz', '(171) 555-2282', '4.1');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Sven', 'Ottlieb', '0241-039123', '4.0');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Annette', 'Roulet', '089-0877310', '3.9');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Yoshi', 'Latimer', '011-4988260', '4.6');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('John', 'Steel', '(503) 555-7555', '4.8');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Jaime', 'Yorres', '(198) 555-8888', '3.7');
INSERT INTO Drivers (FirstName, LastName, Phone, Ratings) VALUES ('Felipe', 'Izquierdo', '(509) 555-7969', '4.3');

INSERT INTO Payments (CustomerID, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
  VALUES('5', 'Cash', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO Payments (CustomerID, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
  VALUES('6', 'Card', 'Master Card', 'Miss Lola Dada', '2233 4321 3345 2220', '8 Johnstown Road', 'London', 'W3 7PG', 'UK');
INSERT INTO Payments (CustomerID, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
  VALUES('2', 'PayPal', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO Payments (CustomerID, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
  VALUES('8', 'ApplePay', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO Payments (CustomerID, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
  VALUES('4', 'Card', 'Debit Card', 'Miss Sherise Wu', '2233 4321 3345 2220', '90 Wadhurst Rd.', 'London', NULL, 'UK');
INSERT INTO Payments (CustomerID, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
  VALUES('3', 'Cash', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
-- INSERT INTO Payments (PaymentNo, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
--   VALUES('12351', 'ApplePay', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
-- INSERT INTO Payments (PaymentNo, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
--   VALUES('12352', 'Cash', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
-- INSERT INTO Payments (PaymentNo, PaymentMethod, CardType, NameOnCard, CardNumber, BillingAddress, BillingCity, BillingPostalCode, BillingCountry)
--   VALUES('12353', 'ApplePay', NULL, NULL, NULL, NULL, NULL, NULL, NULL);


INSERT INTO Orders (RestaurantID, CustomerID, DriverID, OrderDate, RequiredDate, DeliveredDate, DeliverVia, DeliverAddress, DeliverCity, DeliverPostalCode, DeliverCountry, DeliveryDetails)
    VALUES('1', '5', '7', '2019-10-11 12:03:30', '2019-10-11 12:35:30', '2019-10-11 12:40:30', 'Freight', 'Obere Str. 57', 'Berlin', '12209', 'Germany', NULL);
INSERT INTO Orders (RestaurantID, CustomerID, DriverID, OrderDate, RequiredDate, DeliveredDate, DeliverVia, DeliverAddress, DeliverCity, DeliverPostalCode, DeliverCountry, DeliveryDetails)
    VALUES('4', '6', '3', '2019-10-12 17:02:00', '2019-10-12 17:25:00', '2019-10-12 17:25:35', 'Car', '33 Parkside', 'Coventry', 'CV1 2ND', 'UK', 'Ring doorbell');
INSERT INTO Orders (RestaurantID, CustomerID, DriverID, OrderDate, RequiredDate, DeliveredDate, DeliverVia, DeliverAddress, DeliverCity, DeliverPostalCode, DeliverCountry, DeliveryDetails)
    VALUES('3', '2', '6', '2019-10-12 19:30:15', '2019-10-12 20:10:15', '2019-10-12 20:08:15', 'Motorbike', '120 Hanover Sq.', 'London', 'WA1 1DP', 'UK', NULL);
INSERT INTO Orders (RestaurantID, CustomerID, DriverID, OrderDate, RequiredDate, DeliveredDate, DeliverVia, DeliverAddress, DeliverCity, DeliverPostalCode, DeliverCountry, DeliveryDetails)
    VALUES('5', '8', '1', '2019-10-13 11:20:30', '2019-10-13 13:20:30', '2019-10-13 13:10:19', 'Freight', 'C/ Araquil, 67', 'Madrid', '28023', 'Spain', NULL);
INSERT INTO Orders (RestaurantID, CustomerID, DriverID, OrderDate, RequiredDate, DeliveredDate, DeliverVia, DeliverAddress, DeliverCity, DeliverPostalCode, DeliverCountry, DeliveryDetails)
    VALUES('6', '4', '5', '2019-11-02 17:57:43', '2019-11-02 18:37:24', '2019-11-02 18:55:54', 'Car', 'Rochfort Court', 'Bath', 'BA1 2AF', 'UK', NULL);
INSERT INTO Orders (RestaurantID, CustomerID, DriverID, OrderDate, RequiredDate, DeliveredDate, DeliverVia, DeliverAddress, DeliverCity, DeliverPostalCode, DeliverCountry, DeliveryDetails)
    VALUES('2', '3', '9', '2019-11-05 19:10:02', '2019-11-05 19:40:05', '2019-11-05 19:40:13', 'Bicycle', '15 Forester Court', 'Bath', 'BA2 6QZ', 'UK', 'Call mobile');

INSERT INTO Order_Details (OrderID, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentID)
    VALUES('1', '10', '14.0000', '4', '0', 'gluten free', '1');
INSERT INTO Order_Details (OrderID, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentID)
    VALUES('2', '41', '9.8000', '2', '0.50', 'none', '2');
INSERT INTO Order_Details (OrderID, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentID)
    VALUES('3', '15', '10.0000', '12', '0.15', 'halal', '3');
INSERT INTO Order_Details (OrderID, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentID)
    VALUES('4', '14', '3.0000', '5', '0', 'gluten free', '4');
INSERT INTO Order_Details (OrderID, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentID)
    VALUES('5', '21', '12.0000', '4', '0.20', 'none', '5');
INSERT INTO Order_Details (OrderID, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentID)
    VALUES('6', '67', '30.0000', '1', '0.15', 'no pork included', '6');
-- INSERT INTO Order_Details (OrderCode, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentNo)
--     VALUES('10255', '24', '40.0000', '5', '0', 'gluten free', '12351');
-- INSERT INTO Order_Details (OrderCode, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentNo)
--     VALUES('10257', '21', '12.0000', '10', '0.20', 'none', '12352');
-- INSERT INTO Order_Details (OrderCode, ProductID, UnitPrice, Quantity, Discount, AllergyInformation, PaymentNo)
--     VALUES('10261', '27', '55.0000', '3', '0.15', 'none', '12353');
