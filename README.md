Team members:  Sherise, Lola

SQL project information:

    Choice: option 3

    Details:
        Design a database to store information relating to customer orders for a food delivery company.

    Your design should take into account;
        Customers
        Suppliers of food
        Delivery drivers
        Orders/Sales
        Payment records

    For this you will have to work out other possible requirements for the design
    and identify how the company might operate.

    Goal:
    To be able to identify what customer orders what food and from whom.
    I should be able to see a breakdown of the order items and their unit cost
    as well as the quantity orders so that I can check that the order amount
    matches the amount paid including any extras such as the cost of delivery, etc.


  **How to run the database:**

  -- To upload the database to local mysql:   
      ``mysql -u root -pc0nygre < Shelala_foodDelivery.sql``

  -- To run insert the values to the table:
      ``mysql -u root -pc0nygre < Shelala_foodDelivery_values.sql``

  -- To get into mysql terminal:
      ``mysql -u root -p``

  -- In mysql, to check what databases I have:
      ``show databases;``

  -- To use shelala do:  
      ``use shelala;``

  -- To select columns in all tables:
        ``select * from Orders, Customers, Order_Details, Restaurants, Payments, Drivers;``


  **Questions to test the database:**

  #1. Find the address for the customers who live outside the United Kingdom.
        `Select Address, City, PostalCode, Country From Customers Where NOT Country = 'UK';`

  #2. How many customers have ordered in each Restaurant?
      `Select Restaurants.RestaurantName, COUNT(Customers.CustomerID) as 'Number Of Customers'
          From Restaurants, Customers, Orders
              Where Orders.CustomerID = Customers.CustomerID and Orders.RestaurantID = Restaurants.RestaurantID
                  Group By Restaurants.RestaurantID; `

  #3. What is the top 3 drivers rating?
        `Select * From Drivers Order By Ratings DESC Limit 3;`

  #4. What is the most popular cuisine being ordered by the customers?
      `Select r.Cuisine as 'Most popular Cuisine', Count(o.OrderID) as 'Number of Orders'
          From Restaurants as r, Orders as o
              Where o.RestaurantID = r.RestaurantID
                  Group By r.Cuisine
                      Order By Count(o.OrderID) DESC
                          Limit 1; `

  #5. What country does most of the orders come from?
        `Select c.Country, Count(o.OrderID) as 'Number of Orders'
              From Customers as c, Orders as o
                  Where c.CustomerID = o.CustomerID
                      Group By c.Country
                          Order By Count(o.OrderID) DESC;`

  #6. What is the average peak hour of ordering food in the evening?
        `SELECT TIME_FORMAT(SEC_TO_TIME(avg(hour(OrderDate) * 3600 + (minute(OrderDate) * 60) + second(OrderDate))),'%H:%i') as AvgOrderTime
              From Orders
                  Where TIME(OrderDate) > '17:00:00';`
